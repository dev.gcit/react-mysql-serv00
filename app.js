const mysql = require('mysql');
const express = require('express');

const app = express();
// app.use(express.static('public'));
// Have Node serve the files for our built React app
app.use(express.static(path.resolve(__dirname, './client/build')));

app.get('/get', (req, res) => {
  con.query("SELECT * FROM customers", (err, result, fields) => {
    if (err) throw err;
    res.json(result);
  });
});
  
app.get('/insert', (req, res) => {
  var sql = "INSERT INTO customers (name, address) VALUES ('Company Inc', 'Highway 37')";
  con.query(sql, (err, result)=> {
    if (err) throw err;
    res.send(result);
  });
});

var con = mysql.createConnection({
  host: "mysql0.serv00.com",
  user: "m3271_test",
  password: "Test45",
  database: "m3271_test"
});

con.connect((err) => {
  if (err) throw err;
  console.log('Connected!');
  var sql = "SELECT COUNT(*) AS count FROM information_schema.tables WHERE table_schema = 'm3271_test' AND table_name = 'customers'";
  con.query(sql, (err, result) => {
    if (err) throw err;
    if (result[0].count === 0) {
      const sql = "CREATE TABLE customers (id INT AUTO_INCREMENT PRIMARY KEY,name VARCHAR(255), address VARCHAR(255))";
      con.query(sql, (err, result) => {
        if (err) throw err;
        console.log("Table created");
      });
    } else {
      console.log("Table already exists");
    }
    //con.end();
  });
});

app.listen(3000, () => {
  console.log('Server started on port 3000');
});